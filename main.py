from fruits import spanish_fruits, brazilian_fruits, japanese_fruits, popular_fruits
from emails import emails_list


def spanish_and_brazilian_fruits(spanish_fruits, brazilian_fruits):
    return [spanish_fruits.intersection(brazilian_fruits)]


def spanish_and_japan_fruits(spanish_fruits, japanese_fruits):
    return [spanish_fruits & japanese_fruits]


def brazilian_and_japan_fruits(brazilian_fruits, japanese_fruits):
    return [brazilian_fruits & japanese_fruits]


def popular_spanish_or_brazilian_fruits(popular_fruits, spanish_fruits, brazilian_fruits):
    return [(popular_fruits & spanish_fruits) | brazilian_fruits]


def popular_only_spanish_fruits(popular_fruits, spanish_fruits, japanese_fruits, brazilian_fruits):
    return [popular_fruits & (spanish_fruits - (japanese_fruits | brazilian_fruits))] 


def only_yahoo_emails(emails_list):
    return {email for email in emails_list if '@yahoo' in email}


def only_hotmail_emails(emails_list):
    return {email for email in emails_list if '@hotmail' in email}


def only_br_emails(emails_list):
    return {email for email in emails_list if email.endswith('br')}
